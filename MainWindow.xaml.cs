﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oefening_28._5
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        PC pc = new PC();
        Printer printer1 = new Printer("Printer 1");
        Printer printer2 = new Printer("Printer 2");
        Printer printer3 = new Printer("Printer 3");
        Printer printer4 = new Printer("Printer 4");
        int teller = 1;

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            foreach (Printer printer in pc.Printers)
            {
                if (printer.Busy == false) 
                {
                    printer.Busy = pc.PrintAf();
                    teller++;
                    break;
                }
                else if (teller == 5)
                {
                    MessageBox.Show("Er zijn geen printers beschikbaar.");
                    break;
                }
            }

            RefreshLabels();
            RefreshImages();
        }
        private void btnResetPrinter1_Click(object sender, RoutedEventArgs e)
        {
            teller--;
            printer1.Reset();
            RefreshLabels();
            RefreshImages();
        }
        private void btnResetPrinter2_Click(object sender, RoutedEventArgs e)
        {
            teller--;
            printer2.Reset();
            RefreshLabels();
            RefreshImages();
        }
        private void btnResetPrinter3_Click(object sender, RoutedEventArgs e)
        {
            teller--;
            printer3.Reset();
            RefreshLabels();
            RefreshImages();
        }
        private void btnResetPrinter4_Click(object sender, RoutedEventArgs e)
        {
            teller--;
            printer4.Reset();
            RefreshLabels();
            RefreshImages();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            pc.AddPrinter(printer1);
            pc.AddPrinter(printer2);
            pc.AddPrinter(printer3);
            pc.AddPrinter(printer4);
            imgPrinter1.Source = new BitmapImage(new Uri("Images/printer_notbusy.jpg", UriKind.Relative));
            imgPrinter2.Source = new BitmapImage(new Uri("Images/printer_notbusy.jpg", UriKind.Relative));
            imgPrinter3.Source = new BitmapImage(new Uri("Images/printer_notbusy.jpg", UriKind.Relative));
            imgPrinter4.Source = new BitmapImage(new Uri("Images/printer_notbusy.jpg", UriKind.Relative));
            lblStatusPrinter1.Content = printer1.ToString();
            lblStatusPrinter2.Content = printer2.ToString();
            lblStatusPrinter3.Content = printer3.ToString();
            lblStatusPrinter4.Content = printer4.ToString();
        }
        private void RefreshLabels()
        {
            lblStatusPrinter1.Content = printer1.ToString();
            lblStatusPrinter2.Content = printer2.ToString();
            lblStatusPrinter3.Content = printer3.ToString();
            lblStatusPrinter4.Content = printer4.ToString();
        }
        private void RefreshImages()
        {
            int teller = 1;

            foreach (Printer printer in pc.Printers)
            {
                if (printer.Busy)
                {
                    switch (teller)
                    {
                        case 1:
                            imgPrinter1.Source = new BitmapImage(new Uri("Images/printer_busy.gif", UriKind.Relative));
                            break;
                        case 2:
                            imgPrinter2.Source = new BitmapImage(new Uri("Images/printer_busy.gif", UriKind.Relative));
                            break; 
                        case 3:
                            imgPrinter3.Source = new BitmapImage(new Uri("Images/printer_busy.gif", UriKind.Relative));
                            break;
                        case 4:
                            imgPrinter4.Source = new BitmapImage(new Uri("Images/printer_busy.gif", UriKind.Relative));
                            break;
                    }
                }
                else
                {
                    switch (teller)
                    {
                        case 1:
                            imgPrinter1.Source = new BitmapImage(new Uri("Images/printer_notbusy.jpg", UriKind.Relative));
                            break;
                        case 2:
                            imgPrinter2.Source = new BitmapImage(new Uri("Images/printer_notbusy.jpg", UriKind.Relative));
                            break;
                        case 3:
                            imgPrinter3.Source = new BitmapImage(new Uri("Images/printer_notbusy.jpg", UriKind.Relative));
                            break;
                        case 4:
                            imgPrinter4.Source = new BitmapImage(new Uri("Images/printer_notbusy.jpg", UriKind.Relative));
                            break;
                    }
                }
                teller++;
            }
            teller = 1;
        }
    }
}
