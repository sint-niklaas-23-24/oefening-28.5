﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_28._5
{
    class PC
    {
        private List<Printer> _printers = new List<Printer>();

        public PC() 
        { 

        }

        public List<Printer> Printers 
        { 
            get { return _printers; } 
            set { _printers = value; }
        }
        public bool PrintAf()
        {
            return true;
        }
        public void AddPrinter(Printer printer)
        {
            Printers.Add(printer);
        }
    }
}
