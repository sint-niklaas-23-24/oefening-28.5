﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_28._5
{
    class Printer
    {
        private bool _busy;
        private string _naam;

        public Printer(string naam) 
        {
            Naam = naam;
        }
        
        public bool Busy 
        { 
            get { return _busy; } 
            set {  _busy = value; } 
        }
        public string Naam 
        { 
            get { return _naam; } 
            set {  _naam = value; }
        }
        public void Reset()
        {
            _busy = false;
        }
        public override string ToString()
        {
            string opdracht = "";

            if (Busy)
            {
                opdracht = "Bezig met printopdracht.";
            }
            else
            {
                opdracht = "Wachtend op een printopdracht.";
            }

            return Naam + ": " + opdracht;
        }
    }
}
